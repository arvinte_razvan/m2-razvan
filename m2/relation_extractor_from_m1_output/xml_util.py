import xml.etree.ElementTree as ET
def get_content_of_all_inner_nodes(node):
    text = ""
    for child_node in node.iter():
        text += child_node.text.strip() + " "
    return text.strip()

def get_all_children_having_attribute(root, child_attribute, child_attribute_value):
    attribute = ".//*[@" + child_attribute+ "='" + child_attribute_value + "']"
    return root.findall(attribute)
