import json
import gensim
import nltk


def getVerb(key):
    text = nltk.word_tokenize(key)
    for verb in nltk.pos_tag(text):
        if verb[1] == 'VBZ':
            return verb[0]


def ConcatenateSynonyms(jsonPath):
    myJson = json.load(open(jsonPath))
    model = gensim.models.KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True, limit=500000)
    myNewRelations = []
    for keyConcept in myJson:
        for relation in myJson[keyConcept]:
            myNewRelations.append(relation)

        for i in range(0, len(myNewRelations) - 1):
            for j in range(i + 1, len(myNewRelations)):
                key1_rel = myNewRelations[i]
                key2_rel = myNewRelations[j]

                rel1 = getVerb(key1_rel)
                rel2 = getVerb(key2_rel)

                if model.similarity(rel1, rel2) > 0.2:
                    for snd_concept in myJson[keyConcept][key1_rel]:
                        if snd_concept in myJson[keyConcept][key2_rel]:
                            myJson[keyConcept][key2_rel][snd_concept] += model.similarity(rel1, rel2) * myJson[keyConcept][key1_rel][snd_concept]
                            myJson[keyConcept][key1_rel][snd_concept] += model.similarity(rel1, rel2) * myJson[keyConcept][key2_rel][snd_concept]

        myNewRelations[:] = []
    with open(jsonPath, 'w') as outfile:
        json.dump(myJson, outfile)


#def main():
#    ConcatenateSynonyms("file.json")

#main()
